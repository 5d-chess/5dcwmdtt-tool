function process(targetAction = null) {
  initializeGameState();
  var largestAction = 0;
  gameNotations = document.getElementById('uiNotation').value.split('\n');
  for(var i = 0;i < gameNotations.length;i++) {
    if(gameNotations[i].length > 0) {
      var notation = notationStrObj(gameNotations[i]);
      if(targetAction === null || notation.action <= targetAction) {
        gameMoves.push(notation);
        if(largestAction < notation.action) { largestAction = notation.action; }
      }
    }
  }
  if(targetAction === null) { uiConfig.targetAction = largestAction; }
  if(uiConfig.targetAction > largestAction) { uiConfig.targetAction = largestAction; }
  if(uiConfig.targetAction < 1) { uiConfig.targetAction = 0; }
  gameMoves.sort((e1, e2) => {
    if(e1.action === e2.action && e1.color !== e2.color) {
      if(e1.color === 'b') { return 1; }
      return -1;
    }
    return e1.action - e2.action;
  });
  //Preprocess movelist
  for(var i = 0;i < gameMoves.length;i++) {
    if(i > 0) {
      if(Number.isNaN(gameMoves[i].origin.turn)) {
        gameMoves[i].origin.turn = gameMoves[i-1].destination.turn;
      }
      if(Number.isNaN(gameMoves[i].origin.line)) {
        gameMoves[i].origin.line = gameMoves[i-1].destination.line;
      }
    }
    else {
      if(Number.isNaN(gameMoves[i].origin.turn)) {
        gameMoves[i].origin.turn = 1;
      }
      if(Number.isNaN(gameMoves[i].origin.line)) {
        gameMoves[i].origin.line = 0;
      }
    }
    if(Number.isNaN(gameMoves[i].destination.turn)) {
      gameMoves[i].destination.turn = gameMoves[i].origin.turn;
    }
    if(Number.isNaN(gameMoves[i].destination.line)) {
      gameMoves[i].destination.line = gameMoves[i].origin.line;
    }
  }
  //Move Piece
  for(var i = 0;i < gameMoves.length;i++) {
    movePiece(
      gameMoves[i].color,
      sanCoord(gameMoves[i].origin.coord).arr[0],
      sanCoord(gameMoves[i].origin.coord).arr[1],
      gameMoves[i].origin.turn,
      gameMoves[i].origin.line,
      sanCoord(gameMoves[i].destination.coord).arr[0],
      sanCoord(gameMoves[i].destination.coord).arr[1],
      gameMoves[i].destination.turn,
      gameMoves[i].destination.line,
      gameMoves[i].newline
    );
  }
  document.getElementById('uiActionIndicator').innerText = 'Action: ' + uiConfig.targetAction;
  drawCb(viewport, uiConfig.onlyWhite, uiConfig.onlyBlack, uiConfig.flip);
}
