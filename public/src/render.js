function drawCb(containerObj, onlyWhite = false, onlyBlack = false, flip = false) {
  containerObj.removeChildren();
  var multiplier = 300;
  var largestTimelineLength = 0;
  for(var i = 0;i < gameState.length;i++) {
    if(gameState[i].turns.length > largestTimelineLength) { largestTimelineLength = gameState[i].turns.length }
  }
  if(onlyWhite === onlyBlack) {
    for(var i = 0;i < gameState.length;i++) {
      for(var j = 0;j < gameState[i].turns.length;j++) {
        drawSingleCb(gameState[i].turns[j].board, containerObj,
          (window.innerWidth - multiplier * 0.85)/2 - (multiplier) * (largestTimelineLength - (gameState[i].turns[j].turn * 2 - (gameState[i].turns[j].board.isWhite ? 1 : 0))),
          (window.innerHeight - multiplier * 0.85)/2 - (multiplier) * gameState[i].timeline * (flip ? -1 : 1),
          multiplier * 0.85,
          multiplier * 0.85,
          {turn: gameState[i].turns[j].turn, line: gameState[i].timeline},
          flip
        );
      }
    }
  }
  else {
    for(var i = 0;i < gameState.length;i++) {
      for(var j = 0;j < gameState[i].turns.length;j++) {
        if((onlyWhite === gameState[i].turns[j].board.isWhite) || (onlyBlack === !gameState[i].turns[j].board.isWhite)) {
          drawSingleCb(gameState[i].turns[j].board, containerObj,
            (window.innerWidth - multiplier * 0.85)/2 - (multiplier) * (largestTimelineLength - gameState[i].turns[j].turn),
            (window.innerHeight - multiplier * 0.85)/2 - (multiplier) * gameState[i].timeline * (flip ? -1 : 1),
            multiplier * 0.85,
            multiplier * 0.85,
            {turn: gameState[i].turns[j].turn, line: gameState[i].timeline},
            flip
          );
        }
      }
    }
  }
}

function returnPiece(c, x, y, t, l) {
  for(var i = 0;i < gameState.length;i++) {
    for(var j = 0;j < gameState[i].turns.length && gameState[i].timeline === l;j++) {
      if(gameState[i].turns[j].turn === t && gameState[i].turns[j].board.isWhite === (c === 'w')) {
        for(var k = 0;k < gameState[i].turns[j].board.pieces.length;k++) {
          if(sanCoord([x, y]).str === gameState[i].turns[j].board.pieces[k].coord) {
            return gameState[i].turns[j].board.pieces[k];
          }
        }
      }
    }
  }
  return null;
}

function drawHighlight(c, x, y, t, l, colorOverride = null) {
  if(x < 0 || x > 7) { return null; }
  if(y < 0 || y > 7) { return null; }
  for(var i = 0;i < gameState.length;i++) {
    for(var j = 0;j < gameState[i].turns.length && gameState[i].timeline === l;j++) {
      if(gameState[i].turns[j].turn === t && gameState[i].turns[j].board.isWhite === (c === 'w')) {
        var piece = returnPiece(c, x, y, t, l);
        if(colorOverride !== null) {
          gameState[i].turns[j].board.highlight.push({color: colorOverride, coord: sanCoord([x, y]).str});
        }
        else if(piece === null) {
          gameState[i].turns[j].board.highlight.push({color: 'blue', coord: sanCoord([x, y]).str});
        }
        else if(c === piece.name[0]) {
          gameState[i].turns[j].board.highlight.push({color: 'green', coord: sanCoord([x, y]).str});
        }
        else {
          gameState[i].turns[j].board.highlight.push({color: 'red', coord: sanCoord([x, y]).str});
        }
      }
    }
  }
}

function drawMovementDimension(c, x, y, t, l, d, colorOverride = null) {
  /*d is direction vector
  d = {
    x: -1 or 1,
    y: -1 or 1,
    t: -1 or 1,
    l: -1 or 1,
    max: null
  }
  */
  var maxTurn = 1;
  var minTimeline = 0;
  var maxTimeline = 0;
  for(var i = 0;i < gameState.length;i++) {
    if(gameState[i].timeline < minTimeline) { minTimeline = gameState[i].timeline; }
    if(gameState[i].timeline > maxTimeline) { maxTimeline = gameState[i].timeline; }
    for(var j = 0;j < gameState[i].turns.length;j++) {
      if(gameState[i].turns[j].turn > maxTurn) { maxTurn = gameState[i].turns[j].turn; }
    }
  }
  var singleSpace = (x2, y2, t2, l2) => {
    var numberOfDim = Math.abs(d.x + d.y + d.t + d.l);
    var xDiff = Math.abs(x2 - x);
    var yDiff = Math.abs(y2 - y);
    var tDiff = Math.abs(t2 - t);
    var lDiff = Math.abs(l2 - l);
    var dis = (xDiff + yDiff + tDiff + lDiff) / numberOfDim;
    if(
      (xDiff === 0 || xDiff === dis) &&
      (yDiff === 0 || yDiff === dis) &&
      (tDiff === 0 || tDiff === dis) &&
      (lDiff === 0 || lDiff === dis) &&
      (!d.max || dis <= d.max)
    ) {
      drawHighlight(c, x2, y2, t2, l2, colorOverride);
    }
  };
  for(var i = x;i <= 7;i++) {
    for(var j = y;j <= 7;j++) {
      for(var k = t;k <= maxTurn;k++) {
        for(var m = l;m <= maxTimeline;m++) {
          singleSpace(i,j,k,m);
        }
        for(var m = l;m >= minTimeline;m--) {
          singleSpace(i,j,k,m);
        }
      }
      for(var k = t;k >= 1;k--) {
        for(var m = l;m <= maxTimeline;m++) {
          singleSpace(i,j,k,m);
        }
        for(var m = l;m >= minTimeline;m--) {
          singleSpace(i,j,k,m);
        }
      }
    }
    for(var j = y;j >= 0;j--) {
      for(var k = t;k <= maxTurn;k++) {
        for(var m = l;m <= maxTimeline;m++) {
          singleSpace(i,j,k,m);
        }
        for(var m = l;m >= minTimeline;m--) {
          singleSpace(i,j,k,m);
        }
      }
      for(var k = t;k >= 1;k--) {
        for(var m = l;m <= maxTimeline;m++) {
          singleSpace(i,j,k,m);
        }
        for(var m = l;m >= minTimeline;m--) {
          singleSpace(i,j,k,m);
        }
      }
    }
  }
  for(var i = x;i >= 0;i--) {
    for(var j = y;j <= 7;j++) {
      for(var k = t;k <= maxTurn;k++) {
        for(var m = l;m <= maxTimeline;m++) {
          singleSpace(i,j,k,m);
        }
        for(var m = l;m >= minTimeline;m--) {
          singleSpace(i,j,k,m);
        }
      }
      for(var k = t;k >= 1;k--) {
        for(var m = l;m <= maxTimeline;m++) {
          singleSpace(i,j,k,m);
        }
        for(var m = l;m >= minTimeline;m--) {
          singleSpace(i,j,k,m);
        }
      }
    }
    for(var j = y;j >= 0;j--) {
      for(var k = t;k <= maxTurn;k++) {
        for(var m = l;m <= maxTimeline;m++) {
          singleSpace(i,j,k,m);
        }
        for(var m = l;m >= minTimeline;m--) {
          singleSpace(i,j,k,m);
        }
      }
      for(var k = t;k >= 1;k--) {
        for(var m = l;m <= maxTimeline;m++) {
          singleSpace(i,j,k,m);
        }
        for(var m = l;m >= minTimeline;m--) {
          singleSpace(i,j,k,m);
        }
      }
    }
  }
}

function drawMovement(c, p, x, y, t, l) {
  clearHighlights();
  drawHighlight(c, x, y, t, l, 'blue');
  if(p.length === 1) {
    if(p[0] === 'w') {
      drawHighlight(c, x, y+1, t, l);
      drawHighlight(c, x, y, t, l+1);
      if(y === 1) {
        drawHighlight(c, x, y+2, t, l);
      }
      drawHighlight(c, x-1, y+1, t, l, 'red');
      drawHighlight(c, x+1, y+1, t, l, 'red');
      drawHighlight(c, x, y, t-1, l+1, 'red');
      drawHighlight(c, x, y, t+1, l+1, 'red');
    }
    if(p[0] === 'b') {
      drawHighlight(c, x, y-1, t, l);
      drawHighlight(c, x, y, t, l-1);
      if(y === 6) {
        drawHighlight(c, x, y-2, t, l);
      }
      drawHighlight(c, x-1, y-1, t, l, 'red');
      drawHighlight(c, x+1, y-1, t, l, 'red');
      drawHighlight(c, x, y, t-1, l-1, 'red');
      drawHighlight(c, x, y, t+1, l-1, 'red');
    }
    drawHighlight(c, x, y, t-1, l);
    drawHighlight(c, x, y, t+1, l);
  }
  if(p[1] === 'N') {
    drawHighlight(c, x-2, y-1, t, l);
    drawHighlight(c, x-2, y+1, t, l);
    drawHighlight(c, x-2, y, t-1, l);
    drawHighlight(c, x-2, y, t+1, l);
    drawHighlight(c, x-2, y, t, l-1);
    drawHighlight(c, x-2, y, t, l+1);
    drawHighlight(c, x+2, y-1, t, l);
    drawHighlight(c, x+2, y+1, t, l);
    drawHighlight(c, x+2, y, t-1, l);
    drawHighlight(c, x+2, y, t+1, l);
    drawHighlight(c, x+2, y, t, l-1);
    drawHighlight(c, x+2, y, t, l+1);

    drawHighlight(c, x-1, y-2, t, l);
    drawHighlight(c, x-1, y+2, t, l);
    drawHighlight(c, x-1, y, t-2, l);
    drawHighlight(c, x-1, y, t+2, l);
    drawHighlight(c, x-1, y, t, l-2);
    drawHighlight(c, x-1, y, t, l+2);
    drawHighlight(c, x+1, y-2, t, l);
    drawHighlight(c, x+1, y+2, t, l);
    drawHighlight(c, x+1, y, t-2, l);
    drawHighlight(c, x+1, y, t+2, l);
    drawHighlight(c, x+1, y, t, l-2);
    drawHighlight(c, x+1, y, t, l+2);

    drawHighlight(c, x, y-2, t-1, l);
    drawHighlight(c, x, y-2, t+1, l);
    drawHighlight(c, x, y-2, t, l-1);
    drawHighlight(c, x, y-2, t, l+1);
    drawHighlight(c, x, y+2, t-1, l);
    drawHighlight(c, x, y+2, t+1, l);
    drawHighlight(c, x, y+2, t, l-1);
    drawHighlight(c, x, y+2, t, l+1);

    drawHighlight(c, x, y-1, t-2, l);
    drawHighlight(c, x, y-1, t+2, l);
    drawHighlight(c, x, y-1, t, l-2);
    drawHighlight(c, x, y-1, t, l+2);
    drawHighlight(c, x, y+1, t-2, l);
    drawHighlight(c, x, y+1, t+2, l);
    drawHighlight(c, x, y+1, t, l-2);
    drawHighlight(c, x, y+1, t, l+2);

    drawHighlight(c, x, y, t-2, l-1);
    drawHighlight(c, x, y, t-2, l+1);
    drawHighlight(c, x, y, t+2, l-1);
    drawHighlight(c, x, y, t+2, l+1);

    drawHighlight(c, x, y, t-1, l-2);
    drawHighlight(c, x, y, t-1, l+2);
    drawHighlight(c, x, y, t+1, l-2);
    drawHighlight(c, x, y, t+1, l+2);
  }
  if(p[1] === 'B' || p[1] === 'Q') {
    drawMovementDimension(c, x, y, t, l, {x: 1, y: 1, t: 0, l: 0});
    drawMovementDimension(c, x, y, t, l, {x: 1, y: 0, t: 1, l: 0});
    drawMovementDimension(c, x, y, t, l, {x: 1, y: 0, t: 0, l: 1});
    drawMovementDimension(c, x, y, t, l, {x: 0, y: 1, t: 1, l: 0});
    drawMovementDimension(c, x, y, t, l, {x: 0, y: 1, t: 0, l: 1});
    drawMovementDimension(c, x, y, t, l, {x: 0, y: 0, t: 1, l: 1});
  }
  if(p[1] === 'R' || p[1] === 'Q') {
    drawMovementDimension(c, x, y, t, l, {x: 1, y: 0, t: 0, l: 0});
    drawMovementDimension(c, x, y, t, l, {x: 0, y: 1, t: 0, l: 0});
    drawMovementDimension(c, x, y, t, l, {x: 0, y: 0, t: 1, l: 0});
    drawMovementDimension(c, x, y, t, l, {x: 0, y: 0, t: 0, l: 1});
  }
  if(p[1] === 'Q') {
    drawMovementDimension(c, x, y, t, l, {x: 1, y: 1, t: 1, l: 1});
    drawMovementDimension(c, x, y, t, l, {x: 0, y: 1, t: 1, l: 1});
    drawMovementDimension(c, x, y, t, l, {x: 1, y: 0, t: 1, l: 1});
    drawMovementDimension(c, x, y, t, l, {x: 1, y: 1, t: 0, l: 1});
    drawMovementDimension(c, x, y, t, l, {x: 1, y: 1, t: 1, l: 0});
  }
  if(p[1] === 'K') {
    drawMovementDimension(c, x, y, t, l, {x: 1, y: 1, t: 0, l: 0, max: 1});
    drawMovementDimension(c, x, y, t, l, {x: 1, y: 0, t: 1, l: 0, max: 1});
    drawMovementDimension(c, x, y, t, l, {x: 1, y: 0, t: 0, l: 1, max: 1});
    drawMovementDimension(c, x, y, t, l, {x: 0, y: 1, t: 1, l: 0, max: 1});
    drawMovementDimension(c, x, y, t, l, {x: 0, y: 1, t: 0, l: 1, max: 1});
    drawMovementDimension(c, x, y, t, l, {x: 0, y: 0, t: 1, l: 1, max: 1});
    drawMovementDimension(c, x, y, t, l, {x: 1, y: 0, t: 0, l: 0, max: 1});
    drawMovementDimension(c, x, y, t, l, {x: 0, y: 1, t: 0, l: 0, max: 1});
    drawMovementDimension(c, x, y, t, l, {x: 0, y: 0, t: 1, l: 0, max: 1});
    drawMovementDimension(c, x, y, t, l, {x: 0, y: 0, t: 0, l: 1, max: 1});
    drawMovementDimension(c, x, y, t, l, {x: 1, y: 1, t: 1, l: 1, max: 1});
    drawMovementDimension(c, x, y, t, l, {x: 0, y: 1, t: 1, l: 1, max: 1});
    drawMovementDimension(c, x, y, t, l, {x: 1, y: 0, t: 1, l: 1, max: 1});
    drawMovementDimension(c, x, y, t, l, {x: 1, y: 1, t: 0, l: 1, max: 1});
    drawMovementDimension(c, x, y, t, l, {x: 1, y: 1, t: 1, l: 0, max: 1});
  }
  clearDupeHighlights();
  drawCb(viewport, uiConfig.onlyWhite, uiConfig.onlyBlack, uiConfig.flip);
}

function clearHighlights() {
  for(var i = 0;i < gameState.length;i++) {
    for(var j = 0;j < gameState[i].turns.length;j++) {
      gameState[i].turns[j].board.highlight = [];
    }
  }
}

function clearDupeHighlights() {
  for(var i = 0;i < gameState.length;i++) {
    for(var j = 0;j < gameState[i].turns.length;j++) {
      for(var k = 0;k < gameState[i].turns[j].board.highlight.length;k++) {
        for(var l = k;l < gameState[i].turns[j].board.highlight.length;l++) {
          if(l !== k &&
          gameState[i].turns[j].board.highlight[l].color === gameState[i].turns[j].board.highlight[k].color &&
          gameState[i].turns[j].board.highlight[l].coord === gameState[i].turns[j].board.highlight[k].coord) {
            gameState[i].turns[j].board.highlight.splice(l,1);
            l--;
          }
        }
      }
    }
  }
}
