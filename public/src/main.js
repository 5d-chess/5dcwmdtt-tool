var app = new PIXI.Application({
  backgroundColor: stdColor('#deaeea')
});
app.renderer.view.style.position = 'absolute';
app.renderer.view.style.display = 'block';
app.renderer.view.style.zIndex = -1;
app.renderer.resize(window.innerWidth, window.innerHeight);
window.addEventListener('resize', () => {
  app.renderer.resize(window.innerWidth, window.innerHeight);
});

document.body.appendChild(app.view);

var viewport = new Viewport.Viewport({
    screenWidth: window.innerWidth,
    screenHeight: window.innerHeight,
    interaction: app.renderer.plugins.interaction
});
app.stage.addChild(viewport);

viewport
  .drag()
  .pinch()
  .wheel()
  .decelerate();

PIXI.Loader.shared
  .add('w', 'img/w.svg')
  .add('b', 'img/b.svg')
  .add('wN', 'img/wN.svg')
  .add('bN', 'img/bN.svg')
  .add('wB', 'img/wB.svg')
  .add('bB', 'img/bB.svg')
  .add('wR', 'img/wR.svg')
  .add('bR', 'img/bR.svg')
  .add('wQ', 'img/wQ.svg')
  .add('bQ', 'img/bQ.svg')
  .add('wK', 'img/wK.svg')
  .add('bK', 'img/bK.svg')
  .load(() => {
    process();
  });
