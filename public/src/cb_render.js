function stdColor(str) {
  var canvas = document.createElement('canvas');
  var ctx = canvas.getContext('2d');
  ctx.fillStyle = str;
  return '0x' + ctx.fillStyle.substring(1);
}

function drawSingleCb(cbObj, containerObj, x, y, w, h, metadata = {}, flip = false) {
  /* Cb Object Notation
  cbObj = {
    active: Boolean,
    isWhite: Boolean,
    pieces: [
      {
        name: Color (lowercase w or b) + SAN Piece Char ('' for pawn),
        coord: SAN Coord
      }
    ],
    highlight: [
      {
        color: Style string,
        coord: SAN Coord
      }
    ]
  }
  */
  //Draw Board
  var graphicsObj = new PIXI.Graphics();
  graphicsObj.lineStyle(cbObj.active ? 0.05 * (w + h)/2 : 0.01 * (w + h)/2, cbObj.isWhite ? stdColor('white') : stdColor('black'), 1, 1);
  graphicsObj.beginFill(stdColor('khaki'));
  graphicsObj.drawRect(x, y, w, h);
  graphicsObj.lineStyle(0);
  graphicsObj.beginFill(stdColor('olive'));
  for(var i = 0;i < 8;i++) {
    for(var j = 0;j < 8;j++) {
      if((i + j) % 2 === 0 && flip) {
        graphicsObj.drawRect(x + i*(w/8), y + j*(h/8), w/8, h/8);
      }
      if((i + j) % 2 !== 0 && !flip) {
        graphicsObj.drawRect(x + i*(w/8), y + j*(h/8), w/8, h/8);
      }
    }
  }
  graphicsObj.interactive = true;
  graphicsObj.click = (e) => {
    var mx = viewport.toWorld(e.data.global.x, e.data.global.y).x - x;
    var my = viewport.toWorld(e.data.global.x, e.data.global.y).y - y;
    var cx = Math.floor(mx/(w/8));
    var cy = 7 - Math.floor(my/(h/8));
    var piece = null;
    for(var i = 0;i < cbObj.pieces.length;i++) {
      if(cbObj.pieces[i].coord === sanCoord([cx,cy]).str) {
        piece = cbObj.pieces[i].name;
      }
    }
    if(piece) {
      drawMovement(cbObj.isWhite ? 'w' : 'b', piece, cx, cy, metadata.turn, metadata.line);
    }
  }
  containerObj.addChild(graphicsObj);
  //Draw Piece
  for(var i = 0;i < cbObj.pieces.length;i++) {
    var sC = sanCoord(cbObj.pieces[i].coord);
    if(sC.arr[0] >= 0 && sC.arr[0] <= 7 && sC.arr[1] >= 0 && sC.arr[1] <= 7) {
      if(flip) {
        drawPiece(cbObj.pieces[i].name, containerObj, x + (sC.arr[0])*(w/8), y + (sC.arr[1])*(h/8), w/8, h/8);
      }
      else {
        drawPiece(cbObj.pieces[i].name, containerObj, x + (sC.arr[0])*(w/8), y + (7-sC.arr[1])*(h/8), w/8, h/8);
      }
    }
  }
  //Draw Highlight
  var graphicsObj = new PIXI.Graphics();
  for(var i = 0;i < cbObj.highlight.length;i++) {
    var sC = sanCoord(cbObj.highlight[i].coord);
    if(sC.arr[0] >= 0 && sC.arr[0] <= 7 && sC.arr[1] >= 0 && sC.arr[1] <= 7) {
      graphicsObj.beginFill(stdColor(cbObj.highlight[i].color), 0.4);
      if(flip) {
        graphicsObj.drawRect(x + (sC.arr[0])*(w/8), y + (sC.arr[1])*(h/8), w/8, h/8);
      }
      else {
        graphicsObj.drawRect(x + (sC.arr[0])*(w/8), y + (7-sC.arr[1])*(h/8), w/8, h/8);
      }
    }
  }
  containerObj.addChild(graphicsObj);
}
