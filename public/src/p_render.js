function drawPiece(pieceStr, containerObj, x, y, w, h) {
  /* Piece String Notation
  pieceStr = Color (lowercase w or b) + SAN Piece Char ('' for pawn)
  */
  var sprite = new PIXI.Sprite(PIXI.Loader.shared.resources[pieceStr].texture);
  if(typeof sprite._textureID !== 'undefined') {
    sprite.width = w;
    sprite.height = h;
    sprite.x = x;
    sprite.y = y;
    containerObj.addChild(sprite);
  }
}
