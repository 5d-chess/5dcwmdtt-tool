var initGameState = [
  {
    timeline: 0,
    turns: [
      {
        turn: 1,
        board: {
          active: true,
          isWhite: true,
          pieces: [
            {name: 'wR', coord: 'a1'},
            {name: 'wN', coord: 'b1'},
            {name: 'wB', coord: 'c1'},
            {name: 'wQ', coord: 'd1'},
            {name: 'wK', coord: 'e1'},
            {name: 'wB', coord: 'f1'},
            {name: 'wN', coord: 'g1'},
            {name: 'wR', coord: 'h1'},
            {name: 'w', coord: 'a2'},
            {name: 'w', coord: 'b2'},
            {name: 'w', coord: 'c2'},
            {name: 'w', coord: 'd2'},
            {name: 'w', coord: 'e2'},
            {name: 'w', coord: 'f2'},
            {name: 'w', coord: 'g2'},
            {name: 'w', coord: 'h2'},
            {name: 'bR', coord: 'a8'},
            {name: 'bN', coord: 'b8'},
            {name: 'bB', coord: 'c8'},
            {name: 'bQ', coord: 'd8'},
            {name: 'bK', coord: 'e8'},
            {name: 'bB', coord: 'f8'},
            {name: 'bN', coord: 'g8'},
            {name: 'bR', coord: 'h8'},
            {name: 'b', coord: 'a7'},
            {name: 'b', coord: 'b7'},
            {name: 'b', coord: 'c7'},
            {name: 'b', coord: 'd7'},
            {name: 'b', coord: 'e7'},
            {name: 'b', coord: 'f7'},
            {name: 'b', coord: 'g7'},
            {name: 'b', coord: 'h7'}
          ],
          highlight: []
        }
      }
    ]
  }
];

var gameState = clone(initGameState);
var gameNotations = [];
var gameMoves = [];

function sortGamestate() {
  gameState.sort((e1, e2) => e1.timeline - e2.timeline);
  gameState.forEach((e) => {
    e.turns.sort((e1, e2) => e1.turn - e2.turn)
  });
}

function initializeGameState() {
  gameState = clone(initGameState);
  gameNotations = [];
  gameMoves = [];
  sortGamestate();
}

function movePiece(c, x1, y1, t1, l1, x2, y2, t2, l2, nl) {
  var piece = '';
  for(var i = 0;i < gameState.length;i++) {
    for(var j = 0;j < gameState[i].turns.length && gameState[i].timeline === l1;j++) {
      for(var k = 0;k < gameState[i].turns[j].board.pieces.length && gameState[i].turns[j].turn === t1;k++) {
        if(sanCoord([x1, y1]).str === gameState[i].turns[j].board.pieces[k].coord && gameState[i].turns[j].board.isWhite === (c === 'w')) {
          piece = gameState[i].turns[j].board.pieces[k].name;
          newBoard = clone(gameState[i].turns[j]);
          if(!newBoard.board.isWhite) {
            newBoard.turn++;
          }
          if(gameState[i].turns[j].board.active) {
            gameState[i].turns[j].board.active = false;
          }
          newBoard.board.isWhite = !newBoard.board.isWhite;
          newBoard.board.pieces.splice(k,1);
          if(t1 === t2 && l1 === l2) {
            newBoard.board.pieces.push({
              name: piece,
              coord: sanCoord([x2, y2]).str
            });
          }
          gameState[i].turns.push(newBoard);
        }
      }
    }
  }
  sortGamestate();
  if(t1 !== t2 || l1 !== l2) {
    for(var i = 0;i < gameState.length;i++) {
      for(var j = 0;j < gameState[i].turns.length && gameState[i].timeline === l2;j++) {
        if(gameState[i].turns[j].turn === t2 && gameState[i].turns[j].board.isWhite === (c === 'w')) {
          newBoard = clone(gameState[i].turns[j]);
          newBoard.board.pieces.push({
            name: piece,
            coord: sanCoord([x2, y2]).str
          });
          newBoard.board.active = true;
          if(!newBoard.board.isWhite) {
            newBoard.turn++;
          }
          newBoard.board.isWhite = !newBoard.board.isWhite;
          if(j >= gameState[i].turns[j].length - 1) {
            if(gameState[i].turns[j].board.active) {
              gameState[i].turns[j].board.active = false;
            }
            gameState[i].turns.push(newBoard);
          }
          else {
            gameState.push({
              timeline: nl,
              turns: [newBoard]
            });
          }
        }
      }
    }
  }
  killPieces();
  sortGamestate();
}

function killPieces() {
  for(var i = 0;i < gameState.length;i++) {
    for(var j = 0;j < gameState[i].turns.length;j++) {
      for(var k = 0;k < gameState[i].turns[j].board.pieces.length;k++) {
        for(var l = k+1;l < gameState[i].turns[j].board.pieces.length;l++) {
          if(gameState[i].turns[j].board.pieces[k].coord === gameState[i].turns[j].board.pieces[l].coord) {
            gameState[i].turns[j].board.pieces.splice(k,1);
            k--;
            l < gameState[i].turns[j].board.pieces.length;
          }
        }
      }
    }
  }
}
