function sanCoord(input) {
  var res = {
    str: '',
    arr: [0,0]
  };
  if(typeof input === 'string') {
    res.str = input;
    res.arr[0] = input.charCodeAt(0) - 97;
    res.arr[1] = Number(input.charAt(1)) - 1;
  }
  else if(Array.isArray(input)) {
    res.str = String.fromCharCode(input[0] + 97) + (input[1] + 1);
    res.arr = input;
  }
  return res;
}

function notationStrObj(str) {
  var tmpstr = str;
  var res = {
    action: NaN,
    color: null,
    piece: null,
    origin: {
      turn: NaN,
      line: NaN,
      coord: null
    },
    destination: {
      turn: NaN,
      line: NaN,
      coord: null
    },
    newline: NaN,
    capture: false,
    check: false,
    checkmate: false,
    enpassant: false
  };
  res.action = Number(tmpstr.match(/\d+/)[0]);
  tmpstr = tmpstr.replace(/\d+/,'');
  res.color = tmpstr.match(/.\. /)[0][0];
  tmpstr = tmpstr.replace(/.\. /,'');
  res.origin.turn = Number(tmpstr.match(/\d+/)[0]);
  tmpstr = tmpstr.replace(/\d+/,'');
  res.origin.line = tmpstr.match(/[-+]\d:[A-Z]*[a-z]/) ? Number(tmpstr.match(/[-+]\d/)[0]) : 0;
  if(!Number.isNaN(res.origin.line)) {
    tmpstr = tmpstr.replace(/[-+]*\d*:/,'');
  }
  res.piece = tmpstr.match(/[A-Z]/) ? tmpstr.match(/[A-Z]/)[0] : '';
  tmpstr = tmpstr.replace(/[A-Z]/,'');
  res.origin.coord = tmpstr.match(/[a-z]\d/)[0];
  tmpstr = tmpstr.replace(/[a-z]\d/,'');
  res.newline = tmpstr.match(/<[-+]\d+>/) ? Number(tmpstr.match(/[-+]\d+/)[0]) : NaN;
  tmpstr = tmpstr.replace(/<[-+]*\d*/,'');
  res.destination.turn = tmpstr.match(/>\d+/) ? Number(tmpstr.match(/\d+/)[0]) : NaN;
  tmpstr = tmpstr.replace(/>\d*/,'');
  res.destination.line = tmpstr.match(/[-+]\d/) ? Number(tmpstr.match(/[-+]\d/)[0]) : NaN;
  if(!tmpstr.match(/[-+]\d/)) {
    tmpstr = tmpstr.replace(/[-+]*\d*:/,'');
  }
  res.capture = tmpstr.match(/x/) !== null;
  tmpstr = tmpstr.replace(/x/,'');
  res.destination.coord = tmpstr.match(/[a-z]\d/)[0];
  tmpstr = tmpstr.replace(/[a-z]\d/,'');
  res.check = tmpstr.match(/\+/) !== null;
  tmpstr = tmpstr.replace(/\+/,'');
  res.checkmate = tmpstr.match(/#/) !== null;
  tmpstr = tmpstr.replace(/#/,'');
  res.enpassant = tmpstr.match(/e\.p\./) !== null;
  tmpstr = tmpstr.replace(/e\.p\./,'');
  return res;
}

/* Notation
(Action #)(Color). [Turn #][+/- Line #]:(Piece)[Coord]<[+/- New Line #]>[Dest Turn #][Dest +/- Line #]:[Capture][Dest Coord][Check][En Passant]
*/
