var showUi = true;
var uiConfig = {
  targetAction: 1,
  onlyWhite: true,
  onlyBlack: true,
  flip: false
};

document.getElementById('uiButton').onclick = () => {
  showUi = !showUi;
  if(showUi) {
    document.getElementById('uiMain').style.display = 'block';
  }
  else {
    document.getElementById('uiMain').style.display = 'none';
  }
};

document.getElementById('uiPrev').onclick = (e) => {
  uiConfig.targetAction--;
  process(uiConfig.targetAction);
};

document.getElementById('uiLoad').onclick = (e) => {
  process();
};

document.getElementById('uiNext').onclick = (e) => {
  uiConfig.targetAction++;
  process(uiConfig.targetAction);
};

document.getElementById('uiOnlyWhite').oninput = (e) => {
  uiConfig.onlyWhite = e.target.checked;
  process(uiConfig.targetAction);
};

document.getElementById('uiOnlyBlack').oninput = (e) => {
  uiConfig.onlyBlack = e.target.checked;
  process(uiConfig.targetAction);
};

document.getElementById('uiFlip').oninput = (e) => {
  uiConfig.flip = e.target.checked;
  process(uiConfig.targetAction);
};
